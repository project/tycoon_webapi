<?php
/**
 * @file
 *   Administration functions and page callbacks for Tycoon WebAPI.
 */

/**
 * Displays the Admin page for WebAPI Keys
 */
function tycoon_webapi_admin() {
  $page = pager_find_page() * 10;
  $vars['header'] = array(
    array(
      'data' => t('Key ID'),
      'field' => 'kid',
      'sort' => 'asc',
    ), 
    array(
      'data' => t('API Key'),
      'field' => 'api_key',
    ),
    array(
      'data' => t('Secret Key'),
      'field' => 'secret',
    ),
    array(
      'data' => t('Status'),
      'field' => 'status',
    ),
    t('Details'),
  );
  $sort = tablesort_get_sort($vars['header']);
  $order = tablesort_get_order($vars['header']);
  $query = db_select('tycoon_webapi_keys', 't')
    ->fields('t');
  $count_query = $query->countQuery();
  $count = $count_query->execute()->fetchField();  
  $keys = $query->range($page,10)
    ->orderBy($order['sql'], $sort)
    ->orderBy('kid', 'asc')
    ->execute();
    
  $active = array(t('Inactive'), t('Active'));
  foreach($keys->fetchAllAssoc('kid', PDO::FETCH_ASSOC) as $kid => $key) {
    $status = $key['status'];
    $row = array(
      $kid,
      $key['api_key'],
      $key['secret'],
      $active[$status],
      l('Details', 'admin/config/tycoon/webapi/key_details/' . $kid),
    );
    $rows[] = $row;
  }
  $vars['rows'] = $rows;

  pager_default_initialize($count, 10);
  $pager = theme('pager');
  return theme('table', $vars) . $pager;
}

/**
 * Displays details of the API Key
 */
function tycoon_webapi_key_details() {
  $args = func_get_args();
  $kid = array_shift($args);
  
  $key = db_select('tycoon_webapi_keys', 't')
   ->condition('t.kid', $kid)
   ->fields('t')
   ->execute()
   ->fetchAll();
 $key = array_shift($key);
 if(empty($key)) {
   return drupal_not_found();
 }
 $page['#key'] = $key;
 $page['#args'] = $args;
 
 //Key details.
 $active = array(t('Inactive'), t('Active'));
 $key_details = '<strong>' . t('API Key') . ':</strong> ' . $key->api_key . '</br>';
 $key_details .= '<strong>' . t('Secret Key') . ':</strong> ' . $key->secret . '</br>';
 $key_details .= '<strong>' . t('Status') . ':</strong> ' . $active[$key->status] . '</br>';
 
 $page['details'] = array(
  '#type' => 'markup',
  '#markup' => $key_details,
 );
 
 //Session Details
 $table['header'] = array(
   array(
    'data' => t('IP Address'),
    'field' => 'ip',
   ),
   array(
    'data' => t('Closes/Closed'),
    'field' => 'until',
   ),
   array(
    'data' => t('Opened'),
    'field' => 'opened',
    'sort' => 'desc',
   ),
 );
 $sort = tablesort_get_sort($table['header']);
 $order = tablesort_get_order($table['header']);
 $pager = pager_find_page(0) * 5;
 $sess_q = db_select('tycoon_webapi_session', 't')
   ->condition('t.kid', $kid)
   ->orderBy('t.' . $order['sql'], $sort)
   ->fields('t');
 $count_query = $sess_q->countQuery();
 $count = $count_query->execute()->fetchField();
 $sessions = $sess_q->range($pager,5)
   ->execute()
   ->fetchAll();
 pager_default_initialize($count, 5, 0);
 
 foreach ($sessions as $session) {
   $row = array(
      $session->ip,
      format_date($session->until),
      format_date($session->opened),
   );
   $table['rows'][] = $row;
 }
 $table['caption'] = '<h3>' . t('Sessions') . '</h3>' . t('!num Total', array('!num' => $count));
 $table['empty'] = t('There are no sessions that have been opened for this Key Pair.');
 $session_table = theme('table', $table) . theme('pager');
 $page['sessions'] = array(
  '#type' => 'markup',
  '#markup' => $session_table,
 );
 
 //Actions form
 $page['actions'] = drupal_get_form('tycoon_webapi_key_actions', $key);
 
 //Commands Available to this API key.
 $perms = db_select('tycoon_webapi_keys_perm', 'p')
  ->condition('kid', $kid)
  ->fields('p', array('perm', 'status'))
  ->execute()
  ->fetchAllAssoc('perm', PDO::FETCH_ASSOC);
 
 $page['command_perms'] = drupal_get_form('tycoon_webapi_key_perms', $kid, $perms);
 drupal_alter('tycoon_webapi_key_details_page', $page);
 return $page;
}

/**
 * Settings page for Tycoon WebAPI
 */
function tycoon_webapi_settings() {
  $form['tycoon_webapi_session_length'] = array(
    '#type' => 'textfield',
    '#title' => t('Session length'),
    '#default_value' => variable_get('tycoon_webapi_session_length', 60),
    '#size' => 5,
    '#description' => t('The length of time (in seconds) a web API session should be allowed to stay open before needing to authenticate again.')
  );
  $form['tycoon_webapi_key_perm'] = array(
    '#type' => 'radios',
    '#title' => t('How to handle API command Permissions'),
    '#options' => array('allow' => t('Allow All'), 'deny' => t('Deny All')),
    '#default_value' => variable_get('tycoon_webapi_key_perm', NULL),
    '#description' => t('Allow All will allow all commands through unless specifically denied. Deny All will deny all commands unless specifically allowed'),
  );
  return system_settings_form($form);
}

/**
 * Key pair Generation callback
 */
function tycoon_webapi_admin_generate() {
  $key = tycoon_webapi_keygen();
  $key['status'] = 1;
  $kid = db_insert('tycoon_webapi_keys')
    ->fields($key)
    ->execute();
  if (is_numeric($kid)) {
    $vars['items'] = array(
      '<strong>' . t('Key ID') . ':</strong> ' . $kid . ' | ' . l('Details', 'admin/config/tycoon/webapi/key_details/' . $kid),
      '<strong>' . t('API Key') . ':</strong> ' . $key['api_key'],
      '<strong>' . t('Secret Key') . ':</strong> ' . $key['secret']
    );
    $key_list = theme('item_list', $vars);
    drupal_set_message(t('Key pair generated:') . $key_list);
  }
  else {
    drupal_set_message(t('Oops. There was a problem generating a key pair. Please try again.'));
  }
  
  drupal_goto('admin/config/tycoon/webapi/keys');
}
/**
 * Form callback for key actions
 */
function tycoon_webapi_key_actions($form, &$form_state, $key) {
  $form['title'] = array(
    '#type' => 'fieldset',
    '#title' => t('Actions'),
  );
  
  $active = array(t('Activate'), t('Deactivate'));
  $form['title']['status'] = array(
    '#type' => 'submit',
    '#value' => $active[$key->status],
    '#name' => 'status'
  );
  $form['title']['delete'] = array(
    '#type' => 'submit',
    '#value' => t('Delete'),
    '#name' => 'delete'
  );
  $form['title']['clear'] = array(
    '#type' => 'submit',
    '#value' => t('Clear Sessions'),
    '#name' => 'clear'
  );
  $form['key'] = array(
    '#type' => 'value',
    '#value' => $key,
  );
  
  return $form;
}

/**
 * Form submit callback
 */
function tycoon_webapi_key_actions_submit($form, &$form_state) {
  $key = $form_state['values']['key'];
  switch ($form_state['clicked_button']['#name']) {
    case 'status' :
      $active = array(t('Deactivated'), t('Activated'));
      $key->status = (int) !$key->status;
      db_update('tycoon_webapi_keys')
        ->condition('kid', $key->kid)
        ->fields((array)$key)
        ->execute();
      $message = t('Key Pair has been !status.', array('!status' => $active[$key->status]));
      drupal_set_message($message);
      break;
    case 'clear' :
      db_delete('tycoon_webapi_session')
        ->condition('kid', $key->kid)
        ->execute();
      drupal_set_message(t('Sessions cleared.'));
      break;
    case 'delete' :
      db_delete('tycoon_webapi_session')
        ->condition('kid', $key->kid)
        ->execute();
      db_delete('tycoon_webapi_keys')
        ->condition('kid', $key->kid)
        ->execute();
      drupal_set_message(t('Key Pair deleted'));
      $form_state['redirect'] = array('admin/config/tycoon/webapi');
      break;
  }
}

/**
 * Form callback for Key permissions
 */
function tycoon_webapi_key_perms($form, &$form_state, $kid, $perms) {
  $allowed = array('allow' => t('Allowed'), 'deny' => t('Denied'));
  $global_allow = variable_get('tycoon_webapi_key_perm', 'allow');
  $form['caption'] = array(
    '#type' => 'markup',
    '#markup' => t('Commands are currently set to be "!allow" by default', array('!allow' => $allowed[$global_allow])),
  );
  $form['kid'] = array(
    '#type' => 'value',
    '#value' => $kid,
  );
  $form['cmds'] = array(
    '#type' => 'container',
    '#theme' => 'tycoon_webapi_key_perms_table',
    '#tree' => TRUE,
  );
  $commands = tycoon_invoke_all('webapi_info');
  foreach ($commands as $command => $data) {
    if (empty($data['bypass perm']) && empty($data['bypass auth'])) {
      $status = isset($perms[$command]) ? $perms[$command]['status'] : 2;
      $form['cmds'][$command] = array(
        '#type' => 'radios',
        '#title' => $command,
        '#options' => array('Deny', 'Allow', 'Default'),
        '#default_value' => $status,
      );
    }
  }
  
  $form['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save permissions'),
  );
  $form['buttons']['clear'] = array(
    '#type' => 'submit',
    '#value' => t('Clear permissions'),
    '#name' => 'clear',
  );
  return $form;
}

/**
 * Form submit for key permissions
 */
function tycoon_webapi_key_perms_submit($form, &$form_state) {
  $kid = $form_state['values']['kid'];
  //Clear out old values
  db_delete('tycoon_webapi_keys_perm')->condition('kid', $kid)->execute();
  if ($form_state['clicked_button']['#name'] == 'clear') {
    drupal_set_message(t('Command permissions cleared.'));
    return;
  }
  $insert = db_insert('tycoon_webapi_keys_perm')->fields(array('kid', 'perm', 'status'));
  $go = FALSE;
  foreach ($form_state['values']['cmds'] as $cmd => $val) {
    $val = (int)$val;
    if ((int)$val === 0 || $val === 1) {
      $insert->values(array(
        'kid' => $kid,
        'perm' => $cmd,
        'status' => $val,
      ));
      //We have at least one record to insert. So set a flag to execute the sql.
      //otherwise we may try to execute an empty sql and have a PDO exception thrown.
      $go = TRUE;
    }
  }
  if ($go) {
    $insert->execute();
  }
  drupal_set_message(t('Command permissions updated.'));
}
/**
 * Theme function for tycoon_webapi_key_perms_table
 */
function theme_tycoon_webapi_key_perms_table($vars) {
  $form = $vars['form'];
  foreach(element_children($form) as $key) {
    unset($form[$key]['#title'], $form[$key][1]['#title'], $form[$key][0]['#title'], $form[$key][2]['#title']);
    $row = array();
    $row[] = $key;
    $row[] = drupal_render($form[$key][1]);
    $row[] = drupal_render($form[$key][0]);
    $row[] = drupal_render($form[$key][2]);
    
    $tvars['rows'][] = $row;
  }
  $tvars['header'] = array(
    t('Command'),
    t('Allow'),
    t('Deny'),
    t('Default'),
  );
  return theme('table', $tvars) . drupal_render_children($form);
}
