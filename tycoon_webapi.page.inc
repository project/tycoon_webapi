<?php
/**
 * @file
 *   The functions that actually implement the page callbacks for the the 
 */

/**
 * The basic callback page for webAPI commands
 */
function tycoon_webapi_page($command, $method = 'post', $format = 'json') {
  //Check to make sure that data is being passed in an allowed format.
  if (!in_array($method, $command['method']) && !variable_get('tycoon_webapi_debug', 0)) {
    $data = array(
      'error' => array(
        'message' => t('The \'!command\' command does not support the !method method', array('!command' => $command['command'], '!method' => drupal_ucfirst($method)))
      ),
    );
    return tycoon_webapi_return($format, $data);
  }
  
  //Set up our posted data based of the method that was used
  $post = ($method == 'post') ? $_POST : $_GET;
  //Kill the q string in case this was a get request
  unset($post['q']);
  
  //Ensure that at the very least we have an api_key
  if (!isset($post['api_key'])) {
    drupal_add_http_header('Status', '403 Forbidden');
    watchdog('access denied', check_plain($_GET['q']), NULL, WATCHDOG_WARNING);
    $e = new TycoonWebAPIException('Access Denied. You must supply a valid API Key for all calls.', $command['command'], 403);
    $data = array(
      'error' => array(
        'message' => $e->__toString(),
        'code' => $e->code,
        'command' => $e->command,
        )
    );
    return tycoon_webapi_return($format, $data);
  }
  
  //Ensures that we have the proper access to be able to make the call
  if (!isset($command['bypass auth'])) {
    try {
      $checkAuth = tycoon_webapi_command_load('checkAuth');
      $class = $checkAuth['handler'];
      $func = $checkAuth['callback'];
      $class::$func($post['session_key'], $post['api_key'], $command);
    }
    catch (TycoonWebAPIException $e) {
      drupal_add_http_header('Status', '403 Forbidden');
      watchdog('access denied', check_plain($_GET['q']), NULL, WATCHDOG_WARNING);
      $data = array(
        'error' => array(
          'message' => $e->__toString(),
          'code' => $e->code,
          'command' => $e->command,
          )
      );
      return tycoon_webapi_return($format, $data);
    }
  }
  
  //We now check to make sure the required fields have been passed in to our $post variable
  $required = $command['required fields'];
  $available = array_keys($post);
  foreach ($required as $key) {
    if (!in_array($key, $available)) {
      drupal_add_http_header('Status', '400 Bad Request');
      $e = new TycoonWebAPIException('Some data was missing. The requested call did not contain all of the fields nessecary to complete the call.', $command['command'], 400);
      $data = array(
        'error' => array(
          'message' => $e->__toString(),
          'code' => $e->code,
          'command' => $e->command,
        ),
      );
      return tycoon_webapi_return($format, $data);
    }
  }
  
  try {
    $class = $command['handler'];
    $handler = new $class($post['api_key'], $post['session_key']);
    $data = call_user_func(array($handler, $command['callback']), $post);
  }
  catch (TycoonWebAPIException $e) {
   if ($e->code == 403) {
     drupal_add_http_header('Status', '403 Forbidden');
     watchdog('access denied', check_plain($_GET['q']), NULL, WATCHDOG_WARNING);
   }
   $data = array(
     'error' => array(
       'message' => $e->__toString(),
       'code' => $e->code,
       'command' => $command['command'],
     ),
   );
  }
  return tycoon_webapi_return($format, $data);
}

/**
 * The return functions for the webAPI
 */
function tycoon_webapi_return($format, $data, $reset = NULL) {
  $formats = &drupal_static(__FUNCTION__, array());
  if (empty($formats) || $reset) {
    unset($formats);
    $formats = tycoon_invoke_all('webapi_return_info');
    drupal_alter('tycoon_webapi_return_info', $commands);
  }
  if (empty($formats[$format])) {
    return drupal_not_found();
  }
  
  $class = $formats[$format];
  $return = new $class($data);
  return $return->render();
}
