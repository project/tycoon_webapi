<?php
/**
 * Tycoon API functions and hooks
 */

/**
 * Implements hook_tycoon_webapi_info().
 */
function tycoon_webapi_tycoon_webapi_info() {
  $base = array(
    'handler' => 'TycoonWebAPI',
  );
  return array(
    'openSession' => array(
      'callback' => 'open_session',
      'bypass auth' => TRUE,
      'method' => array('post'),
      'required fields' => array('api_key', 'signature'),
      'formats' => array('json', 'php', 'xml'),
    ) + $base,
    'checkAuth' => array(
      'callback' => 'check_auth',
      'bypass perm' => TRUE,
      'method' => array('post'),
      'required fields' => array('api_key', 'session_key'),
      'formats' => array('json', 'php', 'xml'),
    ) + $base,
    'closeSession' => array(
      'callback' => 'close_session',
      'bypass perm' => TRUE,
      'method' => array('post'),
      'formats' => array('json', 'php', 'xml'),
    ) + $base,
    'getTransaction' => array(
      'callback' => 'transaction_load',
      'method' => array('post'),
      'required fields' => array('transaction_id'),
      'formats' => array('json', 'php', 'xml'),
    ) + $base,
    'doTransaction' => array(
      'callback' => 'transaction_do',
      'method' => array('post'),
      'required fields' => array('transaction_id', 'action'),
      'formats' => array('json', 'php', 'xml'),
    ) + $base,
    'batchTransaction' => array(
      'callback' => 'transaction_batch',
      'method' => array('post'),
      'formats' => array('json', 'php', 'xml'),
    ) + $base,
  );
}

/**
 * Implements hook_tycoon_webapi_return_info().
 */
function tycoon_webapi_tycoon_webapi_return_info() { 
  return array(
    'json' => 'tycoon_webapi_json',
    'php' => 'tycoon_webapi_php',
    'xml' => 'tycoon_webapi_xml',
    'csv' => 'tycoon_webapi_csv',
    'dump' => 'tycoon_webapi_formatter_basic',
  );
}

/**
 * Implements hook_tycoon_webapi_extra_actions().
 */
function tycoon_webapi_tycoon_webapi_extra_actions() {
  module_load_include('inc', 'tycoon_webapi', 'tycoon_webapi.extra_actions');
  return array(
    'logActivity' => 'tycoon_webapi_log_activity',
    'addLineItem' => 'tycoon_webapi_add_line_item',
    'setStatus' => 'tycoon_webapi_set_status',
  );
}