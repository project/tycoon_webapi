<?php
/**
 * @file
 *   Schema Definitions and (un)Install functions.
 */

/**
 * Implements hook_install().
 */
function tycoon_webapi_uninstall() { 
  db_delete('variable')
    ->condition('name', db_like('tycoon_webapi') . '%', 'LIKE')
    ->execute();
}

/**
 * Implements hook_schema().
 */
function tycoon_webapi_schema() { 
  $schema['tycoon_webapi_keys'] = array(
    'description' => 'The storage for keys and secret keys',
    'fields' => array(
      'kid' => array(
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => 'Unique Key ID.',
      ),
      'api_key' => array(
        'type' => 'varchar',
        'length' => 64,
        'not null' => TRUE,
        'description' => 'Unique API Key.',
      ),
      'secret' => array(
        'type' => 'varchar',
        'length' => 64,
        'not null' => TRUE,
        'description' => 'Secret Key generated for this API Key.',
      ),
      'status' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'length' => 1,
        'description' => 'Boolean flag of whether or not the key pair is active.',
      ),
    ),
    'primary key' => array('kid'),
    'unique keys' => array(
      'key' => array('api_key'),
    ),
    'indexes' => array(
      'api_key' => array('api_key'),
      'auth' => array('api_key', 'secret'),
    ),
  );
  $schema['tycoon_webapi_keys_perm'] = array(
    'description' => 'The storage for command permissions granted to keys',
    'fields' => array(
      'kid' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => 'Key ID.',
      ),
      'perm' => array(
        'type' => 'varchar',
        'length' => 64,
        'not null' => TRUE,
        'description' => 'Command.',
      ),
      'status' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'length' => 1,
        'description' => 'Boolean flag of whether or not the command is allowed.',
      ),
    ),
    'indexes' => array(
      'permission' => array('kid', 'perm'),
    ),
  );
  $schema['tycoon_webapi_session'] = array(
    'description' => 'The storage for keys and secret keys',
    'fields' => array(
      'ssid' => array(
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => 'Session ID.',
      ),
      'kid' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => 'Unique Key ID.',
      ),
      'session_key' => array(
        'type' => 'varchar',
        'length' => 64,
        'not null' => TRUE,
        'description' => 'Unique Session Key.',
      ),
      'ip' => array(
        'type' => 'varchar',
        'length' => 64,
        'not null' => TRUE,
        'description' => 'IP Address for this Session.',
      ),
      'until' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'description' => 'The Unix Timestap for the time until this session is expired.',
      ),
      'opened' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'description' => 'The Unix Timestap for when this session was opened.',
      ),
    ),
    'primary key' => array('ssid'),
    'unique keys' => array(
      'session_key' => array('session_key'),
    ),
    'indexes' => array(
      'key' => array('session_key'),
      'auth' => array('session_key', 'ip', 'until')
    ),
  );
  return $schema;
}