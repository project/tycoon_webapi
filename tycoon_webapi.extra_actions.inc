<?php
/**
 * @file
 *   Extra actions function for the tycoon webapi
 */

/**
 * Adds a Line item to a transaction.
 */
function tycoon_webapi_add_line_item(&$txn, $post) {
  foreach($post['line_items'] as $line_item) {
    $txn->add_line_item($line_item);
  }
  $txn->save();
}

/**
 * Sets the Transactions state to whatever status is supplied in the 'status' key
 */
function tycoon_webapi_set_status(&$txn, $post) {
  $txn->set_state($post['status']);
  $txn->save();
}

/**
 * Allows an activity to be logged against a transaction via the WebAPIs
 */
function tycoon_webapi_log_activity(&$txn, $post) {
  $txn->log_activity($post['activity']);
  $txn->save();
}
