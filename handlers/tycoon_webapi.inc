<?php
/**
 * @file
 *   The default handler for basic Tycoon WebAPI commands
 */

/**
 * This class handle the most basic actions associated with a Tycoon WebAPI session
 * 
 * It provides the basics for opening a session, checking that a session is valid,
 * and provides two methods for processing transactions(single and batch).
 * 
 * Before any webapi call can be made a session must first be generated.
 * After this session is generated all other calls will require an authorization check.
 * 
 * To ensure that any and all calls are handled correctly modules should extend this class.
 */
class TycoonWebAPI {
  function __construct($api_key = NULL, $session_key = NULL) {
    $this->api_key = $api_key;
    $this->session_key = $session_key;
  }
  
  static function check_auth($session_key, $api_key, $command = array('command' => 'checkAuth')) {
    //Do some stuff to check the session key. RETURN TRUE if it passes.
    $now = time();
    $reason = 'You must open a vaild session.';
    $q = db_select('tycoon_webapi_session', 's')
      ->condition('s.session_key', $session_key)
      ->condition('s.ip', ip_address())
      ->condition('s.until', $now , '>=')
      ->fields('s', array('session_key'));
    $alias = $q->join('tycoon_webapi_keys', 'k', '%alias.kid = s.kid');
    $q->condition($alias . '.api_key', $api_key)
      ->condition($alias . '.status', 1)
      ->fields($alias, array('kid'));
    $check = $q->execute();
    $count = $check->rowCount();
    if (empty($command['bypass perm']) && $count > 0) {
      $kid = $check->fetch()->kid;
      $perm = db_select('tycoon_webapi_keys_perm', 'p')
        ->condition('perm', $command['command'])
        ->condition('kid', $kid)
        ->fields('p', array('status'))
        ->execute()
        ->fetchField();
      $allowed = array('allow' => TRUE, 'deny' => FALSE);
      $global_allow = variable_get('tycoon_webapi_key_perm', 'allow');
      $allowed = $allowed[$global_allow];
      $allow = is_numeric($perm) ? $perm : $allowed;
      $reason = 'You do not have access to the requested command.';
    }
    if ($count > 0 && $allow) {
      return TRUE;
    }
    //If it fails fall through to this.
    throw new TycoonWebAPIException("Authorization failed. " . $reason, $command['command'], 403);
    return FALSE;
  }
  
  /**
   * Command Callbacks
   */
  /**
   * Callback for the openSession command
   * 
   * @param $post
   *   An associative array of posted data. It should contain at the least the following keys
   *    - api_key: The API key for the account accessing the service
   *    - signature: The Secret key signature for the account.
   *   We need the above values for this command because it, by it's very nature, needs to bypass the checkAuth command
   *   that all other commands are run through prior to their being called. These values are what allows a session to be opened.
   * 
   * @return
   *   The session key for the generated session.
   */
  function open_session($post = array()) {
    if (!isset($post['api_key']) || !isset($post['signature'])) {
      throw new TycoonWebAPIException("Authorization failed. The credentials you supplied were not valid", 'OpenSession', 403);
    }
    
    $check = db_select('tycoon_webapi_keys', 't')
      ->condition('t.api_key', $post['api_key'])
      ->condition('t.secret', $post['signature'])
      ->fields('t')
      ->execute();
    if ($check->rowCount() < 1) {
      throw new TycoonWebAPIException("Authorization failed. The credentials you supplied were not valid", 'OpenSession', 403);
      return FALSE;
    }
    return $this->generate_session($check->fetch());
  }
  
  /**
   * Closes an open session.
   */
  function close_session() {
    //The session has already been checked to be valid by checkAuth. If the call has gotten this far
    //Then we can assume it's a valid session.
    $until = time() - 5; //set the until to now minus 5 seconds.
    $check = db_update('tycoon_webapi_session')
      ->condition('session_key', $this->session_key)
      ->fields(array('until' => $until))
      ->execute();
    return (boolean) $check; 
  }
  
  /**
   * Loads up an existing or new transaction
   */
  function transaction_load($post = array()) {
    if (is_numeric($post['transaction_id'])) {
      $txn = tycoon_transaction_load($post['transaction_id']);
      if ($txn != NULL) {
        return $txn;
      }
    }
    throw new TycoonWebAPIException("Transaction not found. Please check to ensure your transaction_id is valid", 'getTransaction');
    return FALSE;
  }
  
  /**
   * Executes a transaction Action.
   */
  function transaction_do($post = array()) {
    try {
      //Try and load the transaction, if there is a problem throw an error.
      $txn = $this->transaction_load($post);
    }
    catch (TycoonWebAPIException $e) {
      throw $e;
      return FALSE;
    }
    
    $actions = explode(',', $post['action']);
    $tycoon_actions = tycoon_get_actions();
    $webapi_actions = tycoon_invoke_all('webapi_extra_actions');
    drupal_alter('tycoon_webapi_extra_actions', $webapi_actions);
    foreach ($actions as $action) {
      try {
        if (isset($tycoon_actions[$action])) {
          $return = $txn->do_action($action, $post['data']);
          if (!$return['status']) {
            throw new TycoonWebAPIException($return['activity']->message, 'doTransaction::' . $action);
          }
          else {
            $txn->save(); 
          }
        }
        elseif (isset($webapi_actions[$action]) && function_exists($webapi_actions[$action])) {
          $func = $webapi_actions[$action];
          $func($txn, $post);
          $txn->save();
        }
        else {
          throw new TycoonWebAPIException('The action you supplied was not a valid action.', 'doTransaction::' . $action);
        }
      }
      catch (TycoonWebAPIException $e){
        throw $e;
        break;
      }
    }
    return $txn;
  }
  
  /**
   * Utility functions
   */
  
  /**
   * Generates a session for an api call
   * 
   * @param $result
   *   The an object containing the kid(Key ID). This is generally recieved from a DB result object
   * 
   * @return
   *   A session token
   */
  function generate_session($result) {
    //set up default values
    $values = array(
      'opened' => time(),
      'until' => time() + variable_get('tycoon_webapi_session_length', 60),
      'ip' => ip_address(),
      'kid' => $result->kid,
    );
    
    $pass = FALSE;
    while(!$pass) {
      $ssid = hash_hmac('sha256', implode(',', $values), uniqid(md5(microtime()), TRUE));
      
      $check = db_select('tycoon_webapi_session', 's')
        ->condition('s.session_key', $ssid)
        ->fields('s', array('session_key'))
        ->execute();
      if ($check->rowCount() < 1) {
        $pass = TRUE;
        $values['session_key'] = $ssid;
      }
    }
    
    $session = db_insert('tycoon_webapi_session')
      ->fields($values)
      ->execute();
    if (!$session) {
      drupal_add_http_header('Status', '500 Internal Server Error');
      throw new TycoonWebAPIException("Session creation failed. The system failed to open a session.", 'OpenSession', 500);
      return FALSE;
    }
    return $ssid;
  }
  
  /**
   * Provides documentation of the various WebAPI commands 
   */
  static function help($command) {
    switch ($command['command']) {
      case 'openSession':
        $doc = $command;
        $doc['required fields'] = array(
          'api_key' => 'The callers unique API Key.',
          'signature' => 'The secret signature of the account',
        );
      break;
    }
  }
}
