<?php

class TycoonWebAPIException extends Exception {
  var $command = '';
  public $code = 0;
  public function __construct($message, $command = '', $code = 0) {
    $this->command = $command;

    // make sure everything is assigned properly
    parent::__construct($message, $code);
  }

  // custom string representation of object
  public function __toString() {
    return  $this->command . ": [{$this->code}]: {$this->message}\n";
  }
}