<?php
/**
 * @file
 *   Formatter base class for tycoon webAPI.
 */

/**
 * Sets up some base processing stuff for a formatter class.
 *
 * Renders the data using a simple print_r
 */
class tycoon_webapi_formatter_basic {
  var $data = array();
  
  function __construct($data) {
    $this->data = $data;
  }
  
  function render() {
    print_r($this->data);
    exit;
  }
}