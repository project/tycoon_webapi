<?php
/**
 * @file
 *   Formatter base class for tycoon webAPI.
 */

/**
 * Sets up some base processing stuff for a formatter class.
 *
 * Renders the data using drupal_json_output
 */
class tycoon_webapi_json extends tycoon_webapi_formatter_basic {
  
  function render() {
    drupal_json_output($this->data);
    exit;
  }
}
